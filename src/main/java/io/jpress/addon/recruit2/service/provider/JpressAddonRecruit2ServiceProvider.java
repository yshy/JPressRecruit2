package io.jpress.addon.recruit2.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.addon.recruit2.service.JpressAddonRecruit2Service;
import io.jpress.addon.recruit2.model.JpressAddonRecruit2;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonRecruit2ServiceProvider extends JbootServiceBase<JpressAddonRecruit2> implements JpressAddonRecruit2Service {

}